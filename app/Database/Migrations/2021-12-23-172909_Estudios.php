<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Estudios extends Migration{

	protected $table      = 'estudios';
	public function up(){

		$this->forge->addField([
			'id_estudio' => [
				'type'              => 'INT',
				'constraint'        => 11,
				'unsigned'          => TRUE,
				'auto_increment'    => TRUE
			],
			'centro_estudios' =>[
				'type'              => 'VARCHAR',
				'constraint'        => '255',
			],

			'inicio' =>[
				'type'              => 'DATETIME',
				'null'              => true
			],

			'fin' =>[
				'type'              => 'DATETIME',
				'null'              => true
			],

			'logro' =>[
				'type'              => 'text',
				'constraint'        => '255',
				'null'              => true
			],

		]);

		$this->forge->addKey('id_estudio', TRUE);
		$this->forge->createTable($this->table);

		$fields = FIELDS_AUDITORIA;
		$this->forge->addColumn($this->table, $fields);

	}

	public function down(){
		$this->forge->dropTable($this->table);
	}
	
}