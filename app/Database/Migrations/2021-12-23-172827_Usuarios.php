<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Usuarios extends Migration{

    protected $table      = 'usuarios';
    public function up(){

        $this->forge->addField([
            'id_usuario' => [
                'type'              => 'INT',
                'constraint'        => 11,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ],
            'nombres' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '255',
            ],

            'ape_pat' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '255',
            ],

            'ape_mat' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '255',
            ],

            'slug' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '255',
            ],

            'email' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '255',
            ],
            
            'foto' =>[
                'type'              => 'INT',
                'constraint'        => 11,
                'null'              => true
            ],

            'banner' =>[
                'type'              => 'INT',
                'constraint'        => 11,
                'null'              => true
            ],

            'sexo' =>[
                'type'              => 'VARCHAR',
                'constraint'        => '255',
                'null'              => true
            ],
        ]);

        $this->forge->addKey('id_usuario', TRUE);
        $this->forge->createTable($this->table);

        $fields = FIELDS_AUDITORIA;
        $this->forge->addColumn($this->table, $fields);

    }

    public function down(){
        $this->forge->dropTable($this->table);
    }
}
