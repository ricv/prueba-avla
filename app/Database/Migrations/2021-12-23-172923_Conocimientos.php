<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Conocimientos extends Migration{
	
	protected $table      = 'conocimientos';
	public function up(){

		$this->forge->addField([
			'id_conocimiento' => [
				'type'              => 'INT',
				'constraint'        => 11,
				'unsigned'          => TRUE,
				'auto_increment'    => TRUE
			],
			'conocimiento' =>[
				'type'              => 'VARCHAR',
				'constraint'        => '255',
			],

			'porcentaje' =>[
				'type'              => 'int',
				'null'              => true
			]

		]);

		$this->forge->addKey('id_conocimiento', TRUE);
		$this->forge->createTable($this->table);

		$fields = FIELDS_AUDITORIA;
		$this->forge->addColumn($this->table, $fields);

	}

	public function down(){
		$this->forge->dropTable($this->table);
	}

}
