<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Experiencia extends Migration{
	
	protected $table      = 'experiencias';
	public function up(){

		$this->forge->addField([
			'id_experiencia' => [
				'type'              => 'INT',
				'constraint'        => 11,
				'unsigned'          => TRUE,
				'auto_increment'    => TRUE
			],
			'empresa' =>[
				'type'              => 'VARCHAR',
				'constraint'        => '255',
			],

			'inicio' =>[
				'type'              => 'DATETIME',
				'null'              => true
			],

			'fin' =>[
				'type'              => 'DATETIME',
				'null'              => true
			],

			'descripcion' =>[
				'type'              => 'text',
				'constraint'        => '255',
				'null'              => true
			],

		]);

		$this->forge->addKey('id_experiencia', TRUE);
		$this->forge->createTable($this->table);

		$fields = FIELDS_AUDITORIA;
		$this->forge->addColumn($this->table, $fields);

	}

	public function down(){
		$this->forge->dropTable($this->table);
	}
}
