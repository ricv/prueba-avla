<?php namespace App\Models;

use CodeIgniter\Model;

class M_experiencias extends Model{

    protected $table      = 'experiencias';
    protected $primaryKey = 'id_experiencias';

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}