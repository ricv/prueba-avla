<?php namespace App\Models;

use CodeIgniter\Model;

class M_conocimientos extends Model
{
    protected $table      = 'conocimientos';
    protected $primaryKey = 'id_conocimiento';

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}